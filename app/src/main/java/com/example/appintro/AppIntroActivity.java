package com.example.appintro;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class AppIntroActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_app_intro);

        setFadeAnimation();

        addSlide(AppIntroFragment.newInstance("First App Intro",
                "First App Intro Details", R.drawable.batman
                , ContextCompat.getColor(getApplicationContext(), R.color.colorAccent)));

        addSlide(AppIntroFragment.newInstance("Second App Intro",
                "Second App Intro Details", R.drawable.icon_15
                , ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));

        addSlide(AppIntroFragment.newInstance("Third App Intro",
                "Third App Intro Details", R.drawable.icon_19
                , ContextCompat.getColor(getApplicationContext(), R.color.colorPrimaryDark)));


    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }

    @Override
    public void finish() {
        super.finish();

    }
}
